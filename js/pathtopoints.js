// All properties needed
var step_point = 10;
var current_svg_xml = "";
var current_svg_width = 0;
var current_svg_height = 0;
var itemInsideDropzone = null;
var dropzone = null;
var msToWaitAfterOverlay = 250;
var data = { paths: [] };

function translatePoint({ x, y }, paper, paper_info) {

  console.log("paper_info.scale = ", paper_info.scale);
  console.log("paper.canvas.clientWidth = ", paper.canvas.clientWidth);
  console.log("paper_info.width = ", paper_info.width);
  console.log("paper.canvas.clientHeight = ", paper.canvas.clientHeight);
  console.log("paper_info.height = ", paper_info.height);
  // return { x, y }
  let translation = {
    x: paper_info.width * paper_info.scale / 2,
    y: paper_info.height * paper_info.scale / 2
  }
  console.log("translation = ", translation);
  let result = {
    x: x - paper_info.width * paper_info.scale / 2,
    y: y - paper_info.height * paper_info.scale / 2
  }
  console.log("result = ", result);

  return result;
}

$(document).ready(function () {
  setupPointsSetting();
  setupGenerationFromText();
  setupCanvas();
  setupDropzone();
  setupDownloadPaths();

  // Directly drop the title logo to debug
  // current_svg_xml = $("#svgTitle")[0].outerHTML;
  // generatePointsFromSvg();
});

function setupCanvas() {
  paper = Raphael(document.getElementById("canvas"), '100%', '100%');
  current_displayed_paths = null;
}

function setupDropzone() {
  document.getElementById('dropzone').addEventListener('drop', manageDropFromTitle, false);

  $('#dropzone').dropzone({
    url: "/upload",
    maxFilesize: 5,
    maxThumbnailFilesize: 1,
    autoProcessQueue: false,
    //acceptedFiles: '.svg',
    init: function () {
      dropzone = this;

      this.on('addedfile', function (file) {
        if (file.type != "image/svg+xml") {
          $.notify("Invalid format, only SVG is supported", "error");
          removeItemFromDropzone();
          return;
        }

        displayHoldOnOverlay("Generating points from SVG");

        removeItemFromDropzone();

        itemInsideDropzone = file;
        read = new FileReader();

        read.readAsBinaryString(file);

        read.onloadend = function () {
          current_svg_xml = read.result;
          setTimeout(generatePointsFromSvg, msToWaitAfterOverlay);
        }
      });
    }
  });
}

function hideHoldOnOverlay() {
  HoldOn.close();
}

function displayHoldOnOverlay(msg) {
  HoldOn.open({ message: msg });
  $(".note").hide();
}

function removeItemFromDropzone() {
  current_svg_xml = "";

  if (itemInsideDropzone != null) {
    dropzone.removeFile(itemInsideDropzone);
    itemInsideDropzone = null;
  }

  $(".dz-preview-manually").remove();
}

function setupGenerationFromText() {
  $('#btn-text').click(function () {
    $("#dialog-generate-from-text").dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: {
        "Generate": function () {
          displayHoldOnOverlay("Generating points from text");
          $(this).dialog("close");

          setTimeout(function () {
            removeItemFromDropzone();

            var font_selected = "fonts/" + $("#fonts").find(":selected").val();
            var url_font = $("#url_font").val();
            var text_svg = $("#text_svg").val();
            var font_size = parseInt($("#font_size").val());

            if (text_svg == "" || isNaN(font_size) || font_size <= 0) {
              $.notify("Invalid fields, do you even UI ?", "error");
              hideHoldOnOverlay();
              return;
            }

            if (url_font == "")
              url_font = font_selected;

            opentype.load(url_font, function (err, font) {
              console.assert(!err, err);
              paths = font.getPaths(text_svg, ($("#dropzone").width() / 2) - (font_size * text_svg.length / 4), font_size, font_size);

              var svgText = "<svg class='dz-preview-manually' width='100%' height='100%'>";
              for (var i = 0; i < paths.length; ++i) {
                svgText += paths[i].toSVG();
              }
              svgText += "</svg>";
              $("#dropzone").append(svgText);

              current_svg_xml = svgText;
              generatePointsFromSvg();

              hideHoldOnOverlay();
            });
          }, msToWaitAfterOverlay);
        },
        "Cancel": function () {
          $(this).dialog("close");
        }
      }
    });
  });
}

function setupPointsSetting() {
  $("#step_point").val(step_point.toString());

  $('#btn-apply').click(function () {
    step_point = parseInt($("#step_point").val());

    if (current_svg_xml == "")
      return;

    displayHoldOnOverlay("Applying new step points length to SVG");

    if (step_point <= 0) step_point = 1;
    setTimeout(generatePointsFromSvg, 500);
  });
}

function bbox_meta(shape, paper_info) {
  let bbox_path = shape.getBBox();
  shape.remove();

  if (paper_info.bbox_top != bbox_path
    && (paper_info.bbox_top.y > bbox_path.y))
    paper_info.bbox_top = bbox_path;
  if (paper_info.bbox_bottom != bbox_path
    && (bbox_path.y + bbox_path.height > paper_info.bbox_bottom.y + paper_info.bbox_bottom.height))
    paper_info.bbox_bottom = bbox_path;
  if (paper_info.bbox_left != bbox_path
    && (paper_info.bbox_left.x > bbox_path.x))
    paper_info.bbox_left = bbox_path;
  if (paper_info.bbox_right != bbox_path
    && (bbox_path.x + bbox_path.width > paper_info.bbox_right.x + paper_info.bbox_right.width))
    paper_info.bbox_right = bbox_path;

  return paper_info;
}

function getInfosFromSVG(paths, polygons, circles) {
  var paper_info = {};
  var initialized = false;
  for (var i = 0; i < paths.length; ++i) {
    var path = $($(paths).get(i)).attr('d').replace(' ', ',');
    var shape = paper.path(path);

    if (!initialized) {
      let bbox_path = shape.getBBox();
      initialized = true;
      paper_info.bbox_top = paper_info.bbox_bottom
        = paper_info.bbox_left = paper_info.bbox_right = bbox_path;
      continue;
    }

    paper_info = bbox_meta(shape, paper_info);
  }

  for (let polygon of polygons) {
    const points = $(polygon).attr('points');
    var shape = paper.path(points)
    paper_info = bbox_meta(shape, paper_info);
  }

  for (let circle of circles) {
    console.log("circle = ", circle);
    var shape = paper.circle(
      $(circle).attr('cx'),
      $(circle).attr('cy'),
      $(circle).attr('r')
    )
    paper_info = bbox_meta(shape, paper_info);
  }

  paper_info.width = (paper_info.bbox_right.x + paper_info.bbox_right.width) - paper_info.bbox_left.x;
  paper_info.height = (paper_info.bbox_bottom.y + paper_info.bbox_bottom.height) - paper_info.bbox_top.y;
  paper_info.x = paper_info.bbox_left.x;
  paper_info.y = paper_info.bbox_top.y;
  if (paper_info.height > paper_info.width)
    paper_info.scale = (paper_info.height > paper.canvas.clientHeight) ? (paper.canvas.clientHeight / paper_info.height) : 1;
  else
    paper_info.scale = (paper_info.width > paper.canvas.clientWidth) ? (paper.canvas.clientWidth / paper_info.width) : 1;

  // console.log(paths_info);
  // Display bboxes used for centering paths
  // var bboxes = [paths_info.bbox_right, paths_info.bbox_left, paths_info.bbox_top, paths_info.bbox_bottom];
  // for (var i = 0; i < 4; ++i) {
  //     var container = paper.rect(bboxes[i].x + 300, bboxes[i].y + 300, bboxes[i].width, bboxes[i].height);
  //     container.attr("stroke", "red");
  // }

  return paper_info;
}

function hexToRgb(hex) {
  if (hex === 'black') return { r: 0, g: 0, b: 0 };
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b;
  });

  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
  } : { r: -1, g: -1, b: -1 };
}


function getColors($el) {
  const rgbStringToRgb = (rgbString) => {
    const rgb = rgbString.replace('rgb(', '').replace(')', '').split(',').map(el => parseFloat(el));
    return rgb[0] ? { r: rgb[0], g: rgb[1], b: rgb[2] } : { r: -1, g: -1, b: -1 };
  }

  let stroke = $el.attr('stroke');
  console.log("stroke = ", stroke);
  if (!stroke) {
    stroke = rgbStringToRgb($el.css('stroke'));
  } else {
    stroke = hexToRgb(stroke);
  }

  let fill = $el.attr('fill');
  if (!fill) {
    fill = rgbStringToRgb($el.css('fill'));
  } else {
    fill = hexToRgb(fill);
  }
  return { stroke, fill };
}

function getPolygonPoints(polygons, offset_x, offset_y, paper, paper_info) {
  for (let i = 0; i < polygons.length; ++i) {

    const id = $($(polygons).get(i)).attr('id');
    data.paths.push(id || `polygon-${i}`)
    let { fill, stroke } = getColors($($(polygons).get(i)));
    const points = $($(polygons).get(i)).attr('points')
      .split(' ').filter(el => el).map(p => {
        return {
          x: parseFloat(p.split(',')[0]),
          y: parseFloat(p.split(',')[1])
        };
      });
    // todo get fill and stroke.
    data[id || `polygon-${i}`] = {
      fill,
      stroke,
      points: points.map(p => translatePoint(p, paper, paper_info))
    };

    const data_points = points.map(p => `${p.x},${p.y}`).join('&#13;');
    var color = randomColor();
    addBelow(id || "Polygon " + i, color, data_points, points.length);
    points.forEach(point => {
      paper.circle(point.x, point.y, 2)
        .attr("fill", color)
        .attr("stroke", "none")
        .transform("T" + offset_x * paper_info.scale + "," + offset_y * paper_info.scale);
    });
  }
}

function getCirclePoints(
  circles, offset_x, offset_y, paper, paper_info) {
  for (let i = 0; i < circles.length; ++i) {
    const id = $($(circles).get(i)).attr('id');
    data.paths.push(id || `circle-${i}`)
    let { fill, stroke } = getColors($($(circles).get(i)));
    const center = {
      x: parseFloat($($(circles).get(i)).attr('cx')),
      y: parseFloat($($(circles).get(i)).attr('cy')),
    };
    const radius = parseFloat($($(circles).get(i)).attr('r'));

    data[id || `circle-${i}`] = {
      fill,
      stroke,
      center: translatePoint(center, paper, paper_info),
      radius,
      points: []
    };

    var color = randomColor();
    addBelow(
      id || "Circle" + i,
      color,
      JSON.stringify({ x: center.x, y: center.y, r: radius }),
      1,
    );
    paper.circle(center.x, center.y, radius)
      .attr("fill", "none")
      .attr("stroke", color)
      .transform("T" + offset_x * paper_info.scale + "," + offset_y * paper_info.scale);
  }
}

function generatePointsFromSvg() {
  paper.clear();
  $('.bellows').remove();
  $('#settings').after("<div class='bellows'></div>");
  $('.bellows').show();

  var parser = new DOMParser();
  var doc = parser.parseFromString(current_svg_xml, "application/xml");
  var paths = doc.getElementsByTagName("path");
  const polygons = doc.getElementsByTagName("polygon");
  const circles = doc.getElementsByTagName("circle");
  current_displayed_paths = paths;

  // Read each paths from svg
  var paper_info = getInfosFromSVG(paths, polygons, circles);
  var offset_path_x = (paper_info.x * paper_info.scale * -1)
    + (paper.canvas.clientWidth / 2)
    - (paper_info.width * paper_info.scale / 2);
  var offset_path_y = (paper_info.y * paper_info.scale * -1) + (paper.canvas.clientHeight / 2) - (paper_info.height * paper_info.scale / 2);
  var all_points = "";
  var all_points_count = 0;
  for (var i = 0; i < paths.length; ++i) {
    var path = $($(paths).get(i)).attr('d').replace(' ', ',');
    let id = $($(paths).get(i)).attr('id');
    let { fill, stroke } = getColors($($(paths).get(i)));

    data.paths.push(id || `path-${i}`)
    data[id || `path-${i}`] = { fill, stroke, points: [] }

    // get points at regular intervals
    var data_points = "";
    var color = randomColor();
    var c;
    for (c = 0; c < Raphael.getTotalLength(path); c += step_point) {
      var point = Raphael.getPointAtLength(path, c);


      data[id || `path-${i}`].points.push(translatePoint(point, paper, paper_info))
      data_points += point.x + "," + point.y + "&#13;";
      var circle = paper.circle(point.x * paper_info.scale, point.y * paper_info.scale, 2)
        .attr("fill", color)
        .attr("stroke", "none")
        .transform("T" + offset_path_x * paper_info.scale + "," + offset_path_y * paper_info.scale);
    }

    all_points_count += c;
    all_points += data_points + "#&#13;";
    addBelow(id || "Path " + i, color, data_points, c / step_point);
  }
  getPolygonPoints(polygons, offset_path_x, offset_path_y, paper, paper_info)
  console.log('circles', circles)
  getCirclePoints(circles, offset_path_x, offset_path_y, paper, paper_info)
  data.translate = {
    x: (paper.canvas.clientWidth / 2 + paper_info.width * paper_info.scale / 2) / 2,
    y: (paper.canvas.clientHeight / 2 + paper_info.height * paper_info.scale / 2) / 2
  }

  addBelow("All Paths", "#2A2A2A", all_points, all_points_count / step_point);

  $('.bellows').bellows();
  hideHoldOnOverlay();
}

function addBelow(name, color, data, nb_pts) {
  var below = "";

  below += "<div class='bellows__item'><div class='bellows__header' style='background-color:" + color + "'>";
  below += name;
  below += "<span>" + nb_pts + " pts</span>";
  below += "</div><div class='bellows__content'>";
  below += "<textarea rows='10' cols='50'>" + data + "</textarea></div></div>";

  $('.bellows').append(below);
}

// Hacky function to manage "fake" drop from image title
function manageDropFromTitle(evt) {
  var svgUrl = evt.dataTransfer.getData('URL');

  // Load local svg file from URL
  if (svgUrl.endsWith("img/TitlePathToPoints.svg")) {
    removeItemFromDropzone();

    $("#dropzone").append("<img class='dz-preview-manually' src='img/TitlePathToPoints.svg'>");

    current_svg_xml = $("#svgTitle")[0].outerHTML;
    generatePointsFromSvg();
  }
}

function setupDownloadPaths() {
  $('#btn-download').click(function () {
    console.log(data)

    // Download a json file of data
    var dataStr = "data:text/json;charset=utf-8,"
      + encodeURIComponent(JSON.stringify(data));
    var dlAnchorElem = document.createElement('a');
    dlAnchorElem.setAttribute("href", dataStr);
    dlAnchorElem.setAttribute("download", "data.json");
    dlAnchorElem.click();
  });
}
